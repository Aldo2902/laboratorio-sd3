﻿using ClienteCLI.Commands.Tareas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteCLI.test.CommandsTest.Tareas
{
    public class CreateTaskCommandTest
    {
        [Test]
        public async Task CrearTarea_Success()
        {
            var httpClient = new HttpClient();
            var createTaskCommand = new CreateTaskCommand();

            async Task Act() => await CreateTaskCommand.CrearTarea("NuevaTarea", "Descripción", DateTime.Now, DateTime.Now.AddDays(1), 1, 1, 1, 1);
            var ex = Assert.ThrowsAsync<HttpRequestException>(Act);

            Assert.That(ex.Message, Contains.Substring("No se puede establecer una conexión"));
        }
    }
}
