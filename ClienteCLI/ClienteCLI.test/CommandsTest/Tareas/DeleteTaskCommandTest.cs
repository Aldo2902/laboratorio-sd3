﻿using ClienteCLI.Commands.Tareas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteCLI.test.CommandsTest.Tareas
{
    public class DeleteTaskCommandTest
    {
        [Test]
        public async Task EliminarTarea_Success()
        {
            var httpClient = new HttpClient();
            var deleteTaskCommand = new DeleteTaskCommand();

            async Task Act() => await DeleteTaskCommand.EliminarTarea(1);
            var ex = Assert.ThrowsAsync<HttpRequestException>(Act);

            Assert.That(ex.Message, Contains.Substring("No se puede establecer una conexión"));
        }
    }
}
