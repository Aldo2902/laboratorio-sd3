﻿using ClienteCLI.Commands.Tareas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteCLI.test.CommandsTest.Tareas
{
    public class UpdateTaskCommandTest
    {
        [Test]
        public async Task ActualizarTarea_Success()
        {
            var httpClient = new HttpClient();
            var updateTaskCommand = new UpdateTaskCommand();

            async Task Act() => await UpdateTaskCommand.ActualizarTarea(1, "NuevoTitulo", "NuevaDescripción", DateTime.Now, DateTime.Now.AddDays(1), 1, 1, 1, 1);
            var ex = Assert.ThrowsAsync<HttpRequestException>(Act);

            Assert.That(ex.Message, Contains.Substring("No se puede establecer una conexión"));
        }
    }
}
