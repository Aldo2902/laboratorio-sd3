﻿using ClienteCLI.Commands.Tareas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteCLI.test.CommandsTest.Tareas
{
    public class GetTaskCommandTest
    {
        [Test]
        public async Task MostrarTareas_Success()
        {
            var httpClient = new HttpClient();
            var getTaskCommand = new GetTaskCommand();

            async Task Act() => await GetTaskCommand.MostrarTareas();
            var ex = Assert.ThrowsAsync<HttpRequestException>(Act);

            Assert.That(ex.Message, Contains.Substring("No se puede establecer una conexión"));
        }
    }
}
