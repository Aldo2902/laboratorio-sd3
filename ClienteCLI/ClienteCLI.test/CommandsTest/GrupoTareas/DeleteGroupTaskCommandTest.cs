﻿using ClienteCLI.Commands.GrupoTareas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteCLI.test.CommandsTest.GrupoTareas
{
    public class DeleteGroupTaskCommandTest
    {
        [Test]
        public async Task EliminarGrupoTarea_Success()
        {
            var httpClient = new HttpClient();
            var deleteGroupTaskCommand = new DeleteGroupTaskCommand();

            async Task Act() => await DeleteGroupTaskCommand.EliminarGrupoTarea(1);
            var ex = Assert.ThrowsAsync<HttpRequestException>(Act);

            Assert.That(ex.Message, Contains.Substring("No se puede establecer una conexión"));
        }
    }
}
