﻿using ClienteCLI.Commands.GrupoTareas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteCLI.test.CommandsTest.GrupoTareas
{
    public class UpdateGroupTaskCommandTest
    {
        [Test]
        public async Task ActualizarGrupoTarea_Success()
        {
            var httpClient = new HttpClient();
            var updateGroupTaskCommand = new UpdateGroupTaskCommand();

            async Task Act() => await UpdateGroupTaskCommand.ActualizarGrupoTarea(1, "NuevoNombre", 2);
            var ex = Assert.ThrowsAsync<HttpRequestException>(Act);

            Assert.That(ex.Message, Contains.Substring("No se puede establecer una conexión"));
        }
    }
}
