﻿using ClienteCLI.Commands.GrupoTareas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteCLI.test.CommandsTest.GrupoTareas
{
    public class CreateGroupTaskCommandTest
    {
        [Test]
        public async Task CrearGrupoTarea_Success()
        {
            async Task Act() => await CreateGroupTaskCommand.CrearGrupoTarea("NuevoGrupo", 1);
            var ex = Assert.ThrowsAsync<HttpRequestException>(Act);

            Assert.That(ex.Message, Contains.Substring("No se puede establecer una conexión"));
        }

    }
}
