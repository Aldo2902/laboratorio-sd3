﻿using ClienteCLI.Commands.GrupoTareas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteCLI.test.CommandsTest.GrupoTareas
{
    public class GetGroupTaskCommandTest
    {
        [Test]
        public async Task MostrarGrupoTareas_Success()
        {
            async Task Act() => await GetGroupTaskCommand.MostrarGrupoTareas();
            var ex = Assert.ThrowsAsync<HttpRequestException>(Act);

            Assert.That(ex.Message, Contains.Substring("No se puede establecer una conexión"));
        }
    }
}
