﻿using ClienteCLI.Models;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.NamingConventionBinder;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteCLI.Commands.GrupoTareas
{
    public class DeleteGroupTaskCommand : BaseCommand
    {
        public override Command CrearComando()
        {
            var command = new Command("eliminar-GrupoTarea", "Eliminar Grupo tarea existente");

            command.AddOption(new Option<int>("-Id", "ID del Grupo de Tarea a eliminar"));

            command.Handler = CommandHandler.Create<int>(EliminarGrupoTarea);

            return command;
        }

        public static async Task EliminarGrupoTarea(int id)
        {
            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.DeleteAsync($"https://localhost:7038/api/GrupoDeTareas/{id}");

                if (response.IsSuccessStatusCode)
                {
                    Console.WriteLine("Grupo de Tarea eliminada exitosamente.");
                }
                else
                {
                    Console.WriteLine("Error al eliminar el grupo de tarea: " + response.ReasonPhrase);
                }
            }
        }
    }
}
