﻿using ClienteCLI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.NamingConventionBinder;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteCLI.Commands.GrupoTareas
{
    public class UpdateGroupTaskCommand : BaseCommand
    {
        public override Command CrearComando()
        {
            var command = new Command("actualizar-Grupotarea", "Actualizar un Grupo de Tarea existente");

            command.AddOption(new Option<int>("-Id", "ID del Grupo de tarea a actualizar"));
            command.AddOption(new Option<string>("-N", "Nombre Nuevo del Grupo de tarea a actualizar"));
            command.AddOption(new Option<int>("-U", "Nuevo ID de usuario asignado al Grupo de tarea"));

            command.Handler = CommandHandler.Create<int, string,int>(ActualizarGrupoTarea);

            return command;
        }

        public static async Task ActualizarGrupoTarea(int Id, string N, int U)
        {
            var grupotareaActualizada = new GroupTask
            {
                Id = Id,
                Nombre = N,
                UsuarioId = U
            };

            using (var httpClient = new HttpClient())
            {
                var json = JsonConvert.SerializeObject(grupotareaActualizada);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await httpClient.PutAsync($"https://localhost:7038/api/GrupoDeTareas/{Id}", content);

                if (response.IsSuccessStatusCode)
                {
                    Console.WriteLine("Grupo de Tarea actualizada exitosamente.");
                }
                else
                {
                    Console.WriteLine("Error al actualizar el Grupo de tarea: " + response.ReasonPhrase);
                }
            }
        }
    }
}
