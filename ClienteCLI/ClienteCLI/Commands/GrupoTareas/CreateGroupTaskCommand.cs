﻿using ClienteCLI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.NamingConventionBinder;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteCLI.Commands.GrupoTareas
{
    public class CreateGroupTaskCommand : BaseCommand
    {
        public override Command CrearComando()
        {
            var command = new Command("crear-GrupoTarea", "Crear una nuevo Grupo tarea");

            command.AddOption(new Option<string>("-N", "Nombre del Grupo de tarea"));
            command.AddOption(new Option<int>("-U", "Usuario ID"));

            command.Handler = CommandHandler.Create<string,int>(CrearGrupoTarea);

            return command;
        }

        public static async Task CrearGrupoTarea(string N, int U)
        {
            var nuevaGroupTask = new GroupTask
            {
                Nombre = N,
                UsuarioId = U,
            };

            using (var httpClient = new HttpClient())
            {
                var json = JsonConvert.SerializeObject(nuevaGroupTask);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await httpClient.PostAsync("https://localhost:7038/api/GrupoDeTareas", content);

                if (response.IsSuccessStatusCode)
                {
                    Console.WriteLine("Grupo de Tarea creada exitosamente.");
                }
                else
                {
                    Console.WriteLine("Error al crear Grupo de tarea: " + response.ReasonPhrase);
                }
            }
        }
    }
}
