﻿using ClienteCLI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.CommandLine.NamingConventionBinder;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteCLI.Commands.GrupoTareas
{
    public class GetGroupTaskCommand : BaseCommand
    {
        public override Command CrearComando()
        {
            var command = new Command("get-GrupoTareas", "Mostrar todas las tareas");
            command.Handler = CommandHandler.Create(MostrarGrupoTareas);
            return command;
        }

        public static async Task MostrarGrupoTareas()
        {
            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.GetAsync("https://localhost:7038/api/GrupoDeTareas");
                if (response.IsSuccessStatusCode)
                {
                    var GroupTaskJson = await response.Content.ReadAsStringAsync();
                    var groupTasks = JsonConvert.DeserializeObject<List<GroupTask>>(GroupTaskJson);
                    Console.WriteLine("Grupo Tareas:");
                    foreach (var grouptask in groupTasks)
                    {
                        Console.WriteLine($"ID: {grouptask.Id}, Nombre: {grouptask.Nombre}, UsuarioId: {grouptask.UsuarioId}");
                    }
                }
                else
                {
                    Console.WriteLine("Error al obtener Grupo de Tareas: " + response.ReasonPhrase);
                }
            }
        }
    }
}
