﻿using ClienteCLI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.CommandLine.NamingConventionBinder;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteCLI.Commands.Tareas
{
    public class UpdateTaskCommand : BaseCommand
    {
        public override Command CrearComando()
        {
            var command = new Command("actualizar-tarea", "Actualizar una tarea existente");

            command.AddOption(new Option<int>("-Id", "ID de la tarea a actualizar"));
            command.AddOption(new Option<string>("-T", "Nuevo título de la tarea"));
            command.AddOption(new Option<string>("-D", "Nueva descripción de la tarea"));
            command.AddOption(new Option<DateTime>("-FC", "Nueva fecha de creación de la tarea"));
            command.AddOption(new Option<DateTime>("-FF", "Nueva fecha de finalización de la tarea"));
            command.AddOption(new Option<int>("-E", "Nuevo estado de la tarea"));
            command.AddOption(new Option<int>("-P", "Nueva prioridad de la tarea"));
            command.AddOption(new Option<int>("-U", "Nuevo ID de usuario asignado a la tarea"));
            command.AddOption(new Option<int>("-GT", "Nuevo ID de grupo de tareas al que pertenece la tarea"));

            command.Handler = CommandHandler.Create<int, string, string, DateTime, DateTime, int, int, int, int>(ActualizarTarea);

            return command;
        }

        public static async Task ActualizarTarea(int id, string T, string D, DateTime FC, DateTime FF, int E, int P, int U, int GT)
        {
            var tareaActualizada = new Tarea
            {
                Id = id,
                Titulo = T,
                Descripcion = D,
                FechaDeCreacion = FC,
                FechaDeFinalizacion = FF,
                Estado = E,
                Prioridad = P,
                UsuarioId = U,
                GrupoDeTareasId = GT
            };

            using (var httpClient = new HttpClient())
            {
                var json = JsonConvert.SerializeObject(tareaActualizada);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await httpClient.PutAsync($"https://localhost:7038/api/Tareas/{id}", content);

                if (response.IsSuccessStatusCode)
                {
                    Console.WriteLine("Tarea actualizada exitosamente.");
                }
                else
                {
                    Console.WriteLine("Error al actualizar tarea: " + response.ReasonPhrase);
                }
            }
        }
    }
}
