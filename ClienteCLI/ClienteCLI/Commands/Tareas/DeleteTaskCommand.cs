﻿using ClienteCLI.Models;
using System;
using System.Collections.Generic;
using System.CommandLine.NamingConventionBinder;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteCLI.Commands.Tareas
{
    public class DeleteTaskCommand : BaseCommand
    {
        public override Command CrearComando()
        {
            var command = new Command("eliminar-tarea", "Eliminar una tarea existente");

            command.AddOption(new Option<int>("-Id", "ID de la tarea a eliminar"));

            command.Handler = CommandHandler.Create<int>(EliminarTarea);

            return command;
        }

        public static async Task EliminarTarea(int id)
        {
            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.DeleteAsync($"https://localhost:7038/api/Tareas/{id}");

                if (response.IsSuccessStatusCode)
                {
                    Console.WriteLine("Tarea eliminada exitosamente.");
                }
                else
                {
                    Console.WriteLine("Error al eliminar tarea: " + response.ReasonPhrase);
                }
            }
        }
    }
}
