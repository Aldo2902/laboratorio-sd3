﻿using ClienteCLI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.CommandLine.NamingConventionBinder;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteCLI.Commands.Tareas
{
    public class GetTaskCommand : BaseCommand
    {
        public override Command CrearComando()
        {
            var command = new Command("get-tareas", "Mostrar todas las tareas");
            command.Handler = CommandHandler.Create(MostrarTareas);
            return command;
        }

        public static async Task MostrarTareas()
        {
            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.GetAsync("https://localhost:7038/api/Tareas");
                if (response.IsSuccessStatusCode)
                {
                    var tareasJson = await response.Content.ReadAsStringAsync();
                    var tareas = JsonConvert.DeserializeObject<List<Tarea>>(tareasJson);
                    Console.WriteLine("Tareas:");
                    foreach (var tarea in tareas)
                    {
                        Console.WriteLine($"ID: {tarea.Id}, Titulo: {tarea.Titulo}, Descripcion: {tarea.Descripcion}");
                    }
                }
                else
                {
                    Console.WriteLine("Error al obtener tareas: " + response.ReasonPhrase);
                }
            }
        }
    }
}
