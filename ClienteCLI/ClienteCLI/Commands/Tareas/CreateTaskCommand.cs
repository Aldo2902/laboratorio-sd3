﻿using ClienteCLI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.CommandLine.NamingConventionBinder;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteCLI.Commands.Tareas
{
    public class CreateTaskCommand : BaseCommand
    {
        public override Command CrearComando()
        {
            var command = new Command("crear-tarea", "Crear una nueva tarea");

            command.AddOption(new Option<string>("-T", "Título de la tarea"));
            command.AddOption(new Option<string>("-D", "Descripción de la tarea"));
            command.AddOption(new Option<DateTime>("-FC", "Fecha de creación de la tarea"));
            command.AddOption(new Option<DateTime>("-FF", "Fecha de finalización de la tarea"));
            command.AddOption(new Option<int>("-E", "Estado de la tarea"));
            command.AddOption(new Option<int>("-P", "Prioridad de la tarea"));
            command.AddOption(new Option<int>("-U", "Usuario ID"));
            command.AddOption(new Option<int>("-GT", "Grupo Tarea ID"));

            command.Handler = CommandHandler.Create<string, string, DateTime, DateTime, int, int,int,int>(CrearTarea);

            return command;
        }

        public static async Task CrearTarea( string T, string D, DateTime FC, DateTime FF, int E, int P, int U, int GT)
        {
            var nuevaTarea = new Tarea
            {
                Titulo = T,
                Descripcion = D,
                FechaDeCreacion = FC,
                FechaDeFinalizacion = FF,
                Estado = E,
                Prioridad = P,
                UsuarioId = U,
                GrupoDeTareasId = GT
            };

            using (var httpClient = new HttpClient())
            {
                var json = JsonConvert.SerializeObject(nuevaTarea);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await httpClient.PostAsync("https://localhost:7038/api/Tareas", content);

                if (response.IsSuccessStatusCode)
                {
                    Console.WriteLine("Tarea creada exitosamente.");
                }
                else
                {
                    Console.WriteLine("Error al crear tarea: " + response.ReasonPhrase);
                }
            }
        }
    }
}
