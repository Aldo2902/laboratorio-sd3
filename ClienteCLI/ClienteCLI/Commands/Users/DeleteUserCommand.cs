﻿using System;
using System.Collections.Generic;
using System.CommandLine.NamingConventionBinder;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClienteCLI.Models;

namespace ClienteCLI.Commands.Users
{
    public class DeleteUserCommand : BaseCommand
    {
        public override Command CrearComando()
        {
            var command = new Command("delete-user", "Eliminar un usuario");
            command.AddOption(new Option<string>("-i", "ID del usuario a eliminar"));

            command.Handler = CommandHandler.Create<string>(EliminarUsuario);
            return command;
        }

        static void EliminarUsuario(string i)
        {
            Console.WriteLine($"Eliminando usuario con ID={i}");
        }
    }
}
