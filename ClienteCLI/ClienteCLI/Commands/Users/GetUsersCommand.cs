﻿using ClienteCLI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.NamingConventionBinder;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace ClienteCLI.Commands.Users
{
    public class GetUsersCommand : BaseCommand
    {
        public override Command CrearComando()
        {
            var command = new Command("get-users", "Mostrar todos los usuarios");
            command.Handler = CommandHandler.Create(MostrarUsuarios);
            return command;
        }

        static async Task MostrarUsuarios()
        {
            /*// Realizar la solicitud HTTP para obtener todos los usuarios
            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.GetAsync("https://localhost:7038/api/Tareas");
                if (response.IsSuccessStatusCode)
                {
                    var tareasJson = await response.Content.ReadAsStringAsync();
                    var tareas = JsonConvert.DeserializeObject<List<Tarea>>(tareasJson);
                    Console.WriteLine("Tareas:");
                    foreach (var tarea in tareas)
                    {
                        Console.WriteLine($"ID: {tarea.Id}, Titulo: {tarea.Titulo}, Descripcion: {tarea.Descripcion}");
                    }
                }
                else
                {
                    Console.WriteLine("Error al obtener Tareas: " + response.ReasonPhrase);
                }
            }*/
        }
    }
}
