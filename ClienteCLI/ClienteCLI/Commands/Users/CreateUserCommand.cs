﻿using ClienteCLI.Models;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.CommandLine.NamingConventionBinder;
using System.CommandLine.Parsing;
using Newtonsoft.Json;
using System.Net.Http;

namespace ClienteCLI.Commands.Users
{
    public class CreateUserCommand : BaseCommand
    {
        public override Command CrearComando()
        {
            var command = new Command("create-user", "Crear un nuevo usuario");

            command.AddOption(new Option<string>("-N", "Nombre del usuario"));
            command.AddOption(new Option<string>("-A", "Apellido del usuario"));
            command.AddOption(new Option<string>("-U", "Nombre de usuario"));

            command.Handler = CommandHandler.Create<string, string, string>(async (N, A, U) =>
            {
                await CrearUsuario(N, A, U);
            });

            return command;
        }


        private async Task CrearUsuario(string nombre, string apellido, string usuario)
        {
           /* try
            {
                // Crear un nuevo objeto Usuario con los datos recibidos
                var nuevoUsuario = new Tarea
                {
                    i = nombre,
                    Apellido = apellido,
                    NombreUsuario = usuario
                };

                // Serializar el objeto Usuario a formato JSON
                var usuarioJson = JsonConvert.SerializeObject(nuevoUsuario);
                var content = new StringContent(usuarioJson, Encoding.UTF8, "application/json");

                // Enviar la solicitud HTTP POST al endpoint de la API
                var response = await _httpClient.PostAsync("https://tu-api.com/usuarios", content);

                if (response.IsSuccessStatusCode)
                {
                    Console.WriteLine("Usuario creado exitosamente.");
                }
                else
                {
                    Console.WriteLine("Error al crear usuario: " + response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error al crear usuario: " + ex.Message);
            }*/
        }
    }
}
