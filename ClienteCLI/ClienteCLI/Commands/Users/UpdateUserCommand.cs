﻿using ClienteCLI.Models;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.NamingConventionBinder;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteCLI.Commands.Users
{
    public class UpdateUserCommand : BaseCommand
    {
        public override Command CrearComando()
        {
            var command = new Command("update-user", "Actualizar un usuario");
            command.AddOption(new Option<string>("-N", "Nuevo nombre del usuario"));
            command.AddOption(new Option<string>("-A", "Nuevo apellido del usuario"));
            command.AddOption(new Option<string>("-U", "Nuevo nombre de usuario"));
            command.AddOption(new Option<string>("-i", "ID del usuario a actualizar"));
            command.Handler = CommandHandler.Create<string, string, string, string>(ActualizarUsuario);
            return command;
        }
        static void ActualizarUsuario(string N, string A, string U, string i)
        {
            Console.WriteLine($"Actualizando usuario con ID={i}: NuevoNombre={N}, NuevoApellido={A}, NuevoUsuario={U}");
        }
    }
}
