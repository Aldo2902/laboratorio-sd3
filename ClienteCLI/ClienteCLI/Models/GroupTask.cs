﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteCLI.Models
{
    public class GroupTask
    {
        public int Id { get; set; }
        public string? Nombre { get; set; }
        public int UsuarioId { get; set; }
    }
}
