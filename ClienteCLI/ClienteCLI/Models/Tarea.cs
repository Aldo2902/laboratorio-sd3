﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteCLI.Models
{
    public class Tarea
    {
        public int Id { get; set; }
        public string? Titulo { get; set; }
        public string? Descripcion { get; set; }
        public DateTime FechaDeCreacion { get; set; }
        public DateTime FechaDeFinalizacion { get; set; }
        public int Estado { get; set; }
        public int Prioridad { get; set; }
        public int UsuarioId { get; set; }
        public int GrupoDeTareasId { get; set; }
    }
}
