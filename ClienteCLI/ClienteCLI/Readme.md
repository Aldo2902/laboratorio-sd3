﻿# Documentación del Cliente CLI

El Cliente CLI es una herramienta de línea de comandos que permite interactuar con una API REST para administrar tareas, usuarios y grupos de tareas.

## Pre Requisito

- Tener en funcionamiento en el servidor la api, que esta consumiento el ClienteCLI
- Puedes clonar la api del siguiente repositorio:
```

```

## instalacion

Para instalar el Cliente CLI, sigue estos pasos:

- Clona el repositorio desde GitHub:
	```
	- git clone https://github.com/tu-usuario/cliente-cli.git
	```
-  Navega al directorio del proyecto:
	```
	- cd ClienteCLI
	```
- Ejecuta el cliente CLI:
	```
	- dotnet run
	```
## Comandos Disponibles
### Usuarios
- get-users: Obtiene la lista de usuarios.
- update-user: Actualiza un usuario existente.
- delete-user: Elimina un usuario existente.
### Tareas
- get-tareas: Obtiene la lista de tareas.
- crear-tarea: Crea una nueva tarea.
- eliminar-tarea: Elimina una tarea existente.
- actualizar-tarea: Actualiza una tarea existente.
### Grupos de Tareas
- get-GrupoTarea: Obtiene la lista de grupos de tareas.
- crear-GrupoTarea: Crea un nuevo grupo de tareas.
- eliminar-GrupoTarea: Elimina un grupo de tareas existente.
- actualizar-GrupoTarea: Actualiza un grupo de tareas existente.

## Uso
Para utilizar el cliente CLI, ejecuta los comandos deseados desde la línea de comandos, seguido de las opciones necesarias.

```
dotnet run get-users
```
- Esto mostrará la lista de usuarios.

```
dotnet run create-task -T "Nueva tarea" -D "Descripción de la tarea" -FC 2024-05-07 -FF 2024-06-08 -E 1 -P 1 -U 1 -GT 1
```

- Esto creará una nueva tarea con los parámetros proporcionados.

















