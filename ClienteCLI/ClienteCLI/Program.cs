﻿// See https://aka.ms/new-console-template for more information
using ClienteCLI.Commands;
using ClienteCLI.Commands.GrupoTareas;
using ClienteCLI.Commands.Tareas;
using ClienteCLI.Commands.Users;
using System;
using System.CommandLine;

namespace ClienteCLI
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");

            var rootCommand = new RootCommand
            {
                new GetUsersCommand().CrearComando(),
                new UpdateUserCommand().CrearComando(),
                new DeleteUserCommand().CrearComando(),
             //   new CreateUserCommand().CrearComando(),

                new GetTaskCommand().CrearComando(),
                new CreateTaskCommand().CrearComando(),
                new DeleteTaskCommand().CrearComando(),
                new UpdateTaskCommand().CrearComando(),

                new GetGroupTaskCommand().CrearComando(),
                new CreateGroupTaskCommand().CrearComando(),
                new DeleteGroupTaskCommand().CrearComando(),
                new UpdateGroupTaskCommand().CrearComando(),

            };

            rootCommand.Invoke(args);
        }
    }
}
