﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Lab3.Models;
using Lab3.Services;

namespace Lab3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GrupoDeTareasController : ControllerBase
    {
        private readonly GrupoDeTareaService _grupoDeTareaService;

        public GrupoDeTareasController(GrupoDeTareaService grupoDeTareaService)
        {
            _grupoDeTareaService = grupoDeTareaService;
        }

        // GET: api/GrupoDeTareas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GrupoDeTareas>>> GetGrupoDeTareas()
        {
            return await _grupoDeTareaService.ObtenerTodosLosGruposDeTareas();
        }

        // GET: api/GrupoDeTareas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<GrupoDeTareas>> GetGrupoDeTareas(int id)
        {
            var grupoDeTareas = await _grupoDeTareaService.ObtenerGrupoDeTareasPorId(id);

            if (grupoDeTareas == null)
            {
                return NotFound();
            }

            return grupoDeTareas;
        }

        // PUT: api/GrupoDeTareas/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGrupoDeTareas(int id, GrupoDeTareas grupoDeTareas)
        {
            if (id != grupoDeTareas.Id)
            {
                return BadRequest();
            }

            try
            {
                await _grupoDeTareaService.ActualizarGrupoDeTareas(id, grupoDeTareas);
            }
            catch (ArgumentException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // POST: api/GrupoDeTareas
        [HttpPost]
        public async Task<ActionResult<GrupoDeTareas>> PostGrupoDeTareas(GrupoDeTareas grupoDeTareas)
        {
            await _grupoDeTareaService.CrearGrupoDeTareas(grupoDeTareas);
            return CreatedAtAction("GetGrupoDeTareas", new { id = grupoDeTareas.Id }, grupoDeTareas);
        }

        // DELETE: api/GrupoDeTareas/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGrupoDeTareas(int id)
        {
            try
            {
                await _grupoDeTareaService.EliminarGrupoDeTareas(id);
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
