﻿using Lab3.Models;
using Microsoft.EntityFrameworkCore;

namespace Lab3.Context
{
    public class AppDbContext:DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            // este constructo permite conectar la base de datos directamente

        }

        public DbSet<Tarea> Tareas { get; set; }
        public DbSet<GrupoDeTareas> GrupoDeTareas { get; set; }
        public DbSet<Usuario> Usuarios { get; set; } 
    }
}
