﻿using Lab3.Services;
using System.Security.Claims;

namespace Lab3.Models
{
    public class Jwt
    {
        public string Key { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string Subject { get; set; }

        public static async Task<dynamic> validarToken(ClaimsIdentity identity, UsuarioService usuarioService)
        {
            try
            {
                if (identity.Claims.Count() == 0)
                {
                    return new
                    {
                        success = false,
                        message = "Verificar si estas enviando un token valido",
                        result = ""
                    };
                }

                var idString = identity.Claims.FirstOrDefault(x => x.Type == "id")?.Value;
                if (!int.TryParse(idString, out int id))
                {
                    return new
                    {
                        success = false,
                        message = "El id en el token no es válido",
                        result = ""
                    };
                }

                Usuario usuario = await usuarioService.ObtenerUsuarioPorId(id);
                if (usuario == null)
                {
                    return new
                    {
                        success = false,
                        message = "El usuario no existe",
                        result = ""
                    };
                }

                return new
                {
                    success = true,
                    message = "exito",
                    result = usuario
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    success = false,
                    message = "Cath: " + ex.Message,
                    result = ""
                };
            }
        }
    }
}
