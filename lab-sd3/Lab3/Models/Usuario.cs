﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Lab3.Models
{
    public class Usuario
    {

        
        public int Id { get; set; }

        [Required(ErrorMessage = "El nombre de usuario es obligatorio.")]
        [StringLength(50, ErrorMessage = "El nombre de usuario debe tener como máximo 50 caracteres.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "La contraseña es obligatoria.")]
        [StringLength(100, ErrorMessage = "La contraseña debe tener como máximo 100 caracteres.")]
        public string Password { get; set; }

        
        [StringLength(30, ErrorMessage = "El correo debe tener como máximo 30 caracteres.")]
        public string Email { get; set; } = null!;
        public virtual ICollection<Tarea> Tareas { get; set; } = new List<Tarea>();
        public virtual ICollection<GrupoDeTareas> GrupoDeTareas { get; set; } = new List<GrupoDeTareas>();
    }
}
