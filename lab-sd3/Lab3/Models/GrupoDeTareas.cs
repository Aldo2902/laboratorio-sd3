﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Lab3.Models
{
    public partial class GrupoDeTareas
    {
        public int Id { get; set; }

        [StringLength(100, ErrorMessage = "El nombre del grupo de tareas debe tener como máximo 100 caracteres.")]
        public string Nombre { get; set; }

        public ICollection<Tarea> Tareas { get; set; } = new List<Tarea>();

        public int UsuarioId { get; set; }
        public virtual Usuario? Usuario { get; set; }
    }
}
