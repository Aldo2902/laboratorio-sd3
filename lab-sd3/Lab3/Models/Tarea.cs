﻿using Lab3.Helpers;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Lab3.Models
{
    public partial class Tarea

    {
            public int Id { get; set; }

            [Required(ErrorMessage = "El título es obligatorio.")]
            [StringLength(100, ErrorMessage = "El título debe tener como máximo 100 caracteres.")]
            public string Titulo { get; set; }

            [StringLength(500, ErrorMessage = "La descripción no puede tener más de 500 caracteres.")]
            public string Descripcion { get; set; }

            [Required(ErrorMessage = "La fecha de creación es obligatoria.")]
            public DateTime FechaDeCreacion { get; set; }

            [Required(ErrorMessage = "La fecha de finalización es obligatoria.")]
            [DateGreaterThan("FechaDeCreacion", ErrorMessage = "La fecha de finalización debe ser posterior a la fecha de creación.")]
            public DateTime FechaDeFinalizacion { get; set; }

            [Required(ErrorMessage = "El estado es obligatorio.")]
            [Column(TypeName = "varchar(20)")]
            public EstadoTarea Estado { get; set; } = EstadoTarea.Pendiente;

            [Required(ErrorMessage = "La prioridad es obligatoria.")]
            [Column(TypeName = "varchar(20)")]
            public PrioridadTarea Prioridad { get; set; } = PrioridadTarea.SinPrioridad;
            
        //--------------------------------------------------------------------------------------//    
            public int UsuarioId { get; set; }
            public virtual Usuario? Usuario { get; set; }

            public int GrupoDeTareasId { get; set; }
            public virtual GrupoDeTareas? GrupoDeTareas { get; set; }
    }
}
