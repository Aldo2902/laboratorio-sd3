# Assigment N4 - Proyect API Task

## Descriptión
This project is a REST API developed in C# using ASP.NET Core, Entity Framework Core and Pomelo.EntityFrameworkCore.MySql. Provides endpoints to manage tasks.

## Project Structure
The project follows an MVC (Model-View-Controller) structure, with the following main folders and files:

- **Models**: Contains the model classes, which represent the entities of the database. Example: `Tarea.cs`.
- **Services**: Contains the service classes that manage the business logic. Example: `TareaService.cs`.
- **Controllers**: Contains the controllers that handle HTTP requests and call the corresponding services. Example: `TareaController.cs`.
- **Context**: Contains the `AppDbContext.cs` class, which represents the context of the database and establishes the relationships between the models.

Additionally, additional files and folders are generated when you run commands such as "Add-Migration" and "update-database" to perform migrations and update the database.

## Setting
1.Since the system is working on a MySQL database, it must create the database with the following script:
- `create database lab5;`

After creating only the database, we follow the following steps
2. Open the project in Visual Studio or any other IDE that supports .NET Core.
3. Make sure you have a MySQL database configured and update the connection string in the `appsettings.json` file if necessary.
- `"AllowedHosts": "*",
"ConnectionStrings": {
  "Connection": "server=localhost;port=3306;database=lab5;uid=root;password=73157942q;"
}`
4. Execute the migration commands to create the database. In this case, since we already have the migration, all that remains is to enter the update command: `Update-database`
5. Start the application by pressing F5 or running `dotnet run` from the terminal. Or you could run the HTTP service to take you to Suawer in visual studio, you could also use the postman tool

## Use
Once the application is up and running, you can send HTTP requests to the endpoints provided by the controllers to perform CRUD operations on the tasks.

## Endpoints
### GET
`https://localhost:7038/api/Tareas`
![img1](Images/img1.png)
### POST
`https://localhost:7038/api/Tareas`
![img2](Images/img2.png)
#### Validacion de fechas
![img3](Images/img3.png)
### GET 
`https://localhost:7038/api/Tareas/{id}`
![img4](Images/img4.png)
### PUT
`https://localhost:7038/api/Tareas/{id}`
![img5](Images/img5.png)
### Delete
`https://localhost:7038/api/Tareas/{id}`
![img6](Images/img6.png)

### Base de Datos
![img7](Images/img7.png)

### Unit Test

![Pruebas](Images/Pruebas.png)
