﻿namespace Lab3.Helpers
{
    public enum PrioridadTarea
    {
        Alta,
        Media,
        Baja,
        Minima,
        SinPrioridad
    }
}
