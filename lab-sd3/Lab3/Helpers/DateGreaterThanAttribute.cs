﻿using System.ComponentModel.DataAnnotations;

namespace Lab3.Helpers
{
    public class DateGreaterThanAttribute: ValidationAttribute
    {
        private readonly string _otherPropertyName;

        public DateGreaterThanAttribute(string otherPropertyName)
        {
            _otherPropertyName = otherPropertyName;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var otherPropertyInfo = validationContext.ObjectType.GetProperty(_otherPropertyName);

            if (otherPropertyInfo == null)
            {
                return new ValidationResult($"La propiedad '{_otherPropertyName}' no existe.");
            }

            var otherPropertyValue = otherPropertyInfo.GetValue(validationContext.ObjectInstance);

            if (!(otherPropertyValue is DateTime))
            {
                return new ValidationResult($"La propiedad '{_otherPropertyName}' no es del tipo DateTime.");
            }

            var otherValue = (DateTime)otherPropertyValue;

            if ((DateTime)value <= otherValue)
            {
                return new ValidationResult(ErrorMessage ?? $"La fecha debe ser posterior a {_otherPropertyName}.");
            }

            return ValidationResult.Success;
        }
    }
}
