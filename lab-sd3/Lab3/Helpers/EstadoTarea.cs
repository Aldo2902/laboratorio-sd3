﻿namespace Lab3.Helpers
{
    public enum EstadoTarea
    {
        Pendiente,
        EnProgreso,
        Completada
    }
}
