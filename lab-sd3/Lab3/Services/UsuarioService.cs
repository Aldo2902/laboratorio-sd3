﻿using Lab3.Context;
using Lab3.Models;
using Microsoft.EntityFrameworkCore;

namespace Lab3.Services
{
    public class UsuarioService
    {
        private readonly AppDbContext _context;

        public UsuarioService(AppDbContext context)
        {
            _context = context;
        }

        public async Task<List<Usuario>> ObtenerTodosLosUsuarios()
        {
            return await _context.Usuarios.ToListAsync();
        }

        public async Task<Usuario> ObtenerUsuarioPorId(int id)
        {
            return await _context.Usuarios.FindAsync(id);
        }

        public async Task<Usuario> CrearUsuario(Usuario usuario)
        {
            _context.Usuarios.Add(usuario);
            await _context.SaveChangesAsync();
            return usuario;
        }

        public async Task ActualizarUsuario(int id, Usuario usuario)
        {
            var existingUser = await _context.Usuarios.FindAsync(id);
            if (existingUser == null)
            {
                throw new KeyNotFoundException($"El Usuario con ID {id} no fue encontrado");
            }

            //_context.Entry(usuario).State = EntityState.Modified;

            existingUser.Name = usuario.Name;
            existingUser.Password = usuario.Password;
            
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsuarioExists(id))
                {
                    throw new KeyNotFoundException($"El Usuario con ID {id} no fue encontrado");
                }
                else
                {
                    throw;
                }
            }
        }

        public async Task EliminarUsuario(int id)
        {
            var usuario = await _context.Usuarios.FindAsync(id);
            if (usuario == null)
            {
                throw new KeyNotFoundException($"El usuario con ID {id} no fue encontrado");
            }

            _context.Usuarios.Remove(usuario);
            await _context.SaveChangesAsync();
        }

        private bool UsuarioExists(int id)
        {
            return _context.Usuarios.Any(e => e.Id == id);
        }
        public async Task<Usuario> Login(string nombreDeUsuario, string contrasena)
        {
            // Buscar el usuario por nombre de usuario (en minúsculas)
            var usuario = await _context.Usuarios.FirstOrDefaultAsync(u => u.Name.ToLower() == nombreDeUsuario.ToLower());

            // Verificar si el usuario existe
            if (usuario != null)
            {
                // Verificar la contraseña
                if (VerificarContrasena(contrasena, usuario.Password))
                {
                    // Si la contraseña es correcta, devolver el usuario
                    return usuario;
                }
            }

            // Si no se encuentra el usuario o la contraseña no es correcta, devolver null
            return null;
        }

        private bool VerificarContrasena(string contrasena, string contrasenaAlmacenada)
        {
            // Aquí deberías implementar la lógica para verificar si la contraseña proporcionada es correcta
            // Puedes utilizar cualquier algoritmo de hash seguro, como bcrypt, para comparar las contraseñas
            // Por ejemplo, si estás utilizando bcrypt, puedes hacer algo como esto:
            // return BCrypt.Net.BCrypt.Verify(contrasena, contrasenaAlmacenada);

            // Para este ejemplo, asumiré una comparación directa
            return contrasena == contrasenaAlmacenada;
        }
    }
}
