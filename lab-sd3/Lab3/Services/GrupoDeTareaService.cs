﻿using Lab3.Context;
using Lab3.Models;
using Microsoft.EntityFrameworkCore;

namespace Lab3.Services
{
    public class GrupoDeTareaService
    {
        private readonly AppDbContext _context;

        public GrupoDeTareaService(AppDbContext context)
        {
            _context = context;
        }

        public async Task<List<GrupoDeTareas>> ObtenerTodosLosGruposDeTareas()
        {
            return await _context.GrupoDeTareas.ToListAsync();
        }

        public async Task<GrupoDeTareas> ObtenerGrupoDeTareasPorId(int id)
        {
            return await _context.GrupoDeTareas.FindAsync(id);
        }

        public async Task<GrupoDeTareas> CrearGrupoDeTareas(GrupoDeTareas grupoDeTareas)
        {
            _context.GrupoDeTareas.Add(grupoDeTareas);
            await _context.SaveChangesAsync();
            return grupoDeTareas;
        }

        public async Task ActualizarGrupoDeTareas(int id, GrupoDeTareas grupoDeTareas)
        {
            if (id != grupoDeTareas.Id)
            {
                throw new ArgumentException("Los ID no coinciden");
            }

            _context.Entry(grupoDeTareas).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GrupoDeTareasExists(id))
                {
                    throw new KeyNotFoundException($"El grupo de tareas con ID {id} no fue encontrado");
                }
                else
                {
                    throw;
                }
            }
        }

        public async Task EliminarGrupoDeTareas(int id)
        {
            var grupoDeTareas = await _context.GrupoDeTareas.FindAsync(id);
            if (grupoDeTareas == null)
            {
                throw new KeyNotFoundException($"El grupo de tareas con ID {id} no fue encontrado");
            }

            _context.GrupoDeTareas.Remove(grupoDeTareas);
            await _context.SaveChangesAsync();
        }

        private bool GrupoDeTareasExists(int id)
        {
            return _context.GrupoDeTareas.Any(e => e.Id == id);
        }
    }
}
