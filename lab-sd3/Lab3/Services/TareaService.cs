﻿using Lab3.Context;
using Lab3.Models;
using Microsoft.EntityFrameworkCore;
namespace Lab3.Services
{
    public class TareaService
    {
        private readonly AppDbContext _context;

        public TareaService(AppDbContext context)
        {
            _context = context;
        }

        public async Task<List<Tarea>> ObtenerTodasLasTareas()
        {
            return await _context.Tareas.ToListAsync();
        }

        public async Task<Tarea> ObtenerTareaPorId(int id)
        {
            return await _context.Tareas.FindAsync(id);
        }

        public async Task<Tarea> CrearTarea(Tarea tarea)
        {
            _context.Tareas.Add(tarea);
            await _context.SaveChangesAsync();
            return tarea;
        }

        public async Task ActualizarTarea(int id, Tarea tarea)
        {
            var existingTarea = await _context.Tareas.FindAsync(id);
            if (existingTarea == null)
            {
                throw new KeyNotFoundException($"La tarea con ID {id} no fue encontrada");
            }

            existingTarea.Titulo = tarea.Titulo;
            existingTarea.Descripcion = tarea.Descripcion;
            existingTarea.FechaDeCreacion = tarea.FechaDeCreacion;
            existingTarea.FechaDeFinalizacion = tarea.FechaDeFinalizacion;
            existingTarea.Estado = tarea.Estado;
            existingTarea.Prioridad = tarea.Prioridad;
            existingTarea.UsuarioId = tarea.UsuarioId;
            existingTarea.GrupoDeTareasId = tarea.GrupoDeTareasId;


            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TareaExists(id))
                {
                    throw new KeyNotFoundException($"La tarea con ID {id} no fue encontrada");
                }
                else
                {
                    throw;
                }
            }
        }

        public async Task EliminarTarea(int id)
        {
            var tarea = await _context.Tareas.FindAsync(id);
            if (tarea == null)
            {
                throw new KeyNotFoundException($"La tarea con ID {id} no fue encontrada");
            }

            _context.Tareas.Remove(tarea);
            await _context.SaveChangesAsync();
        }

        private bool TareaExists(int id)
        {
            return _context.Tareas.Any(e => e.Id == id);
        }
    }
}
