﻿using Lab3.Context;
using Lab3.Helpers;
using Lab3.Models;
using Lab3.Services;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.test.Services
{
    [TestFixture]
    public class TareaServiceTest
    {
        private AppDbContext _context;
        private TareaService _tareaService;
        [SetUp]
        public void Setup()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: "TestDatabase")
                .Options;

            _context = new AppDbContext(options);
            _tareaService = new TareaService(_context);
        }
        [Test]
        public async Task GetTareas()
        {
            // Arrange
            await _context.Tareas.AddRangeAsync(new List<Tarea>
        {
        new Tarea {
            Id = 9,
            Titulo = "Tarea 1",
            Descripcion = "Descripción de la Tarea 1",
            FechaDeCreacion = new DateTime(2024, 4, 17),
            FechaDeFinalizacion = new DateTime(2024, 6, 17),
            Estado = EstadoTarea.EnProgreso,
            Prioridad = PrioridadTarea.Baja,
            UsuarioId = 1,
            Usuario = null,
            GrupoDeTareasId = 2,
            GrupoDeTareas = null
        },
        new Tarea {
            Id = 11, // Cambia el ID a uno único, por ejemplo, 11
            Titulo = "Realizar compras", // Cambia el título para que sea diferente
            Descripcion = "ir de compra al hipermaxi", // Cambia la descripción para que sea diferente
            FechaDeCreacion = new DateTime(2024, 5, 18), // Cambia la fecha de creación si es necesario
            FechaDeFinalizacion = new DateTime(2024, 7, 18), // Cambia la fecha de finalización si es necesario
            Estado = EstadoTarea.EnProgreso,
            Prioridad = PrioridadTarea.Baja,
            UsuarioId = 1,
            Usuario = null,
            GrupoDeTareasId = 2,
            GrupoDeTareas = null
        }
    });
            await _context.SaveChangesAsync();

            // Act
            var resultado = await _tareaService.ObtenerTodasLasTareas();

            // Assert
            Assert.IsNotNull(resultado);
            Assert.AreEqual(5, resultado.Count);
        }

        [Test]
        public async Task GetTareaID()
        {
            // Arrange
            var tarea = 
                new Tarea {
                    Id = 10,
                    Titulo = "Tarea 1",
                    Descripcion = "Descripción de la Tarea 1",
                    FechaDeCreacion = new DateTime(2024, 4, 17),
                    FechaDeFinalizacion = new DateTime(2024, 6, 17),
                    Estado = EstadoTarea.EnProgreso,
                    Prioridad = PrioridadTarea.Baja,
                    UsuarioId = 1,
                    Usuario = null,
                    GrupoDeTareasId = 2,
                    GrupoDeTareas = null
                };
            _context.Tareas.Add(tarea);
            await _context.SaveChangesAsync();

            // Act
            var resultado = await _tareaService.ObtenerTareaPorId(10);

            // Assert
            Assert.IsNotNull(resultado);
            Assert.AreEqual(tarea.Titulo, resultado.Titulo);
            Assert.AreEqual(tarea.Descripcion, resultado.Descripcion);
            Assert.AreEqual(tarea.FechaDeCreacion, resultado.FechaDeCreacion);
            Assert.AreEqual(tarea.FechaDeFinalizacion, resultado.FechaDeFinalizacion);
            Assert.AreEqual(tarea.Estado, resultado.Estado);
            Assert.AreEqual(tarea.Prioridad, resultado.Prioridad);
            Assert.AreEqual(tarea.UsuarioId, resultado.UsuarioId);
            Assert.AreEqual(tarea.Usuario, resultado.Usuario);
            Assert.AreEqual(tarea.GrupoDeTareasId, resultado.GrupoDeTareasId);
            Assert.AreEqual(tarea.GrupoDeTareas, resultado.GrupoDeTareas);
            

        }
        [Test]
        public async Task CrearTarea()
        {
            // Arrange
            var nuevaTarea = new Tarea
            {
                Id = 5,
                Titulo = "Resolver Tarea De Calculo",
                Descripcion = "Resolver esto hasta el martes",
                FechaDeCreacion = new DateTime(2024, 4, 17),
                FechaDeFinalizacion = new DateTime(2024, 6, 17),
                Estado = EstadoTarea.EnProgreso,
                Prioridad = PrioridadTarea.Baja,
                UsuarioId = 1,
                Usuario = null,
                GrupoDeTareasId = 2,
                GrupoDeTareas = null
            };
            // Act
            var resultado = await _tareaService.CrearTarea(nuevaTarea);

            // Assert
            Assert.IsNotNull(resultado);
            Assert.AreEqual(nuevaTarea.Titulo, resultado.Titulo);
            Assert.AreEqual(nuevaTarea.Descripcion, resultado.Descripcion);
            Assert.AreEqual(nuevaTarea.FechaDeCreacion, resultado.FechaDeCreacion);
            Assert.AreEqual(nuevaTarea.FechaDeFinalizacion, resultado.FechaDeFinalizacion);
            Assert.AreEqual(nuevaTarea.Estado, resultado.Estado);
            Assert.AreEqual(nuevaTarea.Prioridad, resultado.Prioridad);
            Assert.AreEqual(nuevaTarea.UsuarioId, resultado.UsuarioId);
            Assert.AreEqual(nuevaTarea.Usuario, resultado.Usuario);
            Assert.AreEqual(nuevaTarea.GrupoDeTareasId, resultado.GrupoDeTareasId);
            Assert.AreEqual(nuevaTarea.GrupoDeTareas, resultado.GrupoDeTareas);

            var tareaEnBaseDeDatos = await _context.Tareas.FirstOrDefaultAsync(t => t.Id == resultado.Id);
            Assert.IsNotNull(tareaEnBaseDeDatos);
            Assert.AreEqual(nuevaTarea.Titulo, resultado.Titulo);
            Assert.AreEqual(nuevaTarea.Descripcion, resultado.Descripcion);
            Assert.AreEqual(nuevaTarea.FechaDeCreacion, resultado.FechaDeCreacion);
            Assert.AreEqual(nuevaTarea.FechaDeFinalizacion, resultado.FechaDeFinalizacion);
            Assert.AreEqual(nuevaTarea.Estado, resultado.Estado);
            Assert.AreEqual(nuevaTarea.Prioridad, resultado.Prioridad);
            Assert.AreEqual(nuevaTarea.UsuarioId, resultado.UsuarioId);
            Assert.AreEqual(nuevaTarea.Usuario, resultado.Usuario);
            Assert.AreEqual(nuevaTarea.GrupoDeTareasId, resultado.GrupoDeTareasId);
            Assert.AreEqual(nuevaTarea.GrupoDeTareas, resultado.GrupoDeTareas);
        }

        [Test]
        public async Task ActualizarTarea()
        {
            // Arrange
            var tarea =
                new Tarea
                {
                    Id = 13,
                    Titulo = "Resolver Tarea De Fisica",
                    Descripcion = "Resolver esto hasta el Miercoles",
                    FechaDeCreacion = new DateTime(2024, 4, 17),
                    FechaDeFinalizacion = new DateTime(2024, 6, 17),
                    Estado = EstadoTarea.EnProgreso,
                    Prioridad = PrioridadTarea.Baja,
                    UsuarioId = 1,
                    Usuario = null,
                    GrupoDeTareasId = 2,
                    GrupoDeTareas = null
                };
            // Agregar la tarea directamente al contexto
            _context.Tareas.Add(tarea);
            await _context.SaveChangesAsync(); // Guardar cambios para que la tarea esté disponible en el contexto

            var tareaActualizada =
                new Tarea
                {
                    Id = 13,
                    Titulo = "Resolver Tarea De Fisicaaa",
                    Descripcion = "Resolver esto hasta el Miercolessss",
                    FechaDeCreacion = new DateTime(2024, 4, 17),
                    FechaDeFinalizacion = new DateTime(2024, 6, 17),
                    Estado = EstadoTarea.EnProgreso,
                    Prioridad = PrioridadTarea.Baja,
                    UsuarioId = 1,
                    Usuario = null,
                    GrupoDeTareasId = 2,
                    GrupoDeTareas = null
                };

            // Act
            await _tareaService.ActualizarTarea(13, tareaActualizada);

            // Assert
            var tareaEnBaseDeDatos = await _context.Tareas.FindAsync(13);
            Assert.IsNotNull(tareaEnBaseDeDatos);
            Assert.AreEqual(tareaActualizada.Titulo, tareaEnBaseDeDatos.Titulo);
            Assert.AreEqual(tareaActualizada.Descripcion, tareaEnBaseDeDatos.Descripcion);
            Assert.AreEqual(tareaActualizada.FechaDeCreacion, tareaEnBaseDeDatos.FechaDeCreacion);
            Assert.AreEqual(tareaActualizada.FechaDeFinalizacion, tareaEnBaseDeDatos.FechaDeFinalizacion);
            Assert.AreEqual(tareaActualizada.Estado, tareaEnBaseDeDatos.Estado);
            Assert.AreEqual(tareaActualizada.Prioridad, tareaEnBaseDeDatos.Prioridad);
            Assert.AreEqual(tareaActualizada.UsuarioId, tareaEnBaseDeDatos.UsuarioId);
            Assert.AreEqual(tareaActualizada.Usuario, tareaEnBaseDeDatos.Usuario);
            Assert.AreEqual(tareaActualizada.GrupoDeTareasId, tareaEnBaseDeDatos.GrupoDeTareasId);
            Assert.AreEqual(tareaActualizada.GrupoDeTareas, tareaEnBaseDeDatos.GrupoDeTareas);
        }

        [Test]
        public async Task EliminarTarea()
        {
            var tarea = 
                new Tarea {
                    Id = 1,
                    Titulo = "Resolver Tarea De Fisicaaa",
                    Descripcion = "Resolver esto hasta el Miercolessss",
                    FechaDeCreacion = new DateTime(2024, 4, 17),
                    FechaDeFinalizacion = new DateTime(2024, 6, 17),
                    Estado = EstadoTarea.EnProgreso,
                    Prioridad = PrioridadTarea.Baja,
                    UsuarioId = 1,
                    GrupoDeTareasId = 2
                };
            _context.Tareas.Add(tarea);
            await _context.SaveChangesAsync();

            await _tareaService.EliminarTarea(1);

            var tareaEnBaseDeDatos = await _context.Tareas.FindAsync(1);
            Assert.IsNull(tareaEnBaseDeDatos);
        }
    }
}
