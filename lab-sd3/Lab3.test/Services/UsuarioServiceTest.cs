﻿using Lab3.Context;
using Lab3.Models;
using Lab3.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.test.Services
{
    [TestFixture]
    public class UsuarioServiceTest
    {
        private AppDbContext _context;
        private UsuarioService _usuarioService;

        [SetUp]
        public void Setup()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: "TestDatabase")
                .Options;

            _context = new AppDbContext(options);
            _usuarioService = new UsuarioService(_context);
        }

        [Test]
        public async Task ObtenerTodosLosUsuarios()
        {
            _context.Usuarios.RemoveRange(_context.Usuarios);
            await _context.SaveChangesAsync();

            var resultado = await _usuarioService.ObtenerTodosLosUsuarios();

            Assert.IsNotNull(resultado);
            Assert.IsEmpty(resultado);
        }
        [Test]
        public async Task ObtenerUsuarioPorId()
        {
            var usuario = new Usuario
            {
                Id = 2,
                Name = "Juan Pepe",
                Password = "123456"
            };
            _context.Usuarios.Add(usuario);
            await _context.SaveChangesAsync();

            var resultado = await _usuarioService.ObtenerUsuarioPorId(2);

            Assert.IsNotNull(resultado);
            Assert.AreEqual(usuario.Id, resultado.Id);
            Assert.AreEqual(usuario.Name, resultado.Name);
            Assert.AreEqual(usuario.Password, resultado.Password);
        }

        [Test]
        public async Task CrearUsuario_DeberiaCrearUsuarioCorrectamente()
        {
            var nuevoUsuario = new Usuario
            {
                Id = 1,
                Name = "Juan Pepe",
                Password = "123456"
            };

            var resultado = await _usuarioService.CrearUsuario(nuevoUsuario);

            Assert.IsNotNull(resultado);
            Assert.AreNotEqual(0, resultado.Id);
            Assert.AreEqual(nuevoUsuario.Name, resultado.Name);
            Assert.AreEqual(nuevoUsuario.Password , resultado.Password);
        }
        [Test]
        public async Task ActualizarUsuario()
        {
            var usuario = new Usuario
            {
                Id = 3,
                Name = "Juan Pepe",
                Password = "123456"
            };
            _context.Usuarios.Add(usuario);
            await _context.SaveChangesAsync();

            var usuarioActualizado = new Usuario
            {
                Id = 3,
                Name = "Juan Pepeee",
                Password = "12345678"
            };

            await _usuarioService.ActualizarUsuario(3, usuarioActualizado);

            // Assert
            var usuarioEnBaseDeDatos = await _context.Usuarios.FindAsync(3);
            Assert.IsNotNull(usuarioEnBaseDeDatos);
            Assert.AreEqual(usuarioActualizado.Name, usuarioEnBaseDeDatos.Name);
            Assert.AreEqual(usuarioActualizado.Password, usuarioEnBaseDeDatos.Password);
        }
        [Test]
        public async Task EliminarUsuario()
        {
            var usuario = new Usuario
            {
                Id = 5,
                Name = "Juan Pepeee",
                Password = "12345678"
            };
            _context.Usuarios.Add(usuario);
            await _context.SaveChangesAsync();

            await _usuarioService.EliminarUsuario(3);

            var usuarioEnBaseDeDatos = await _context.Usuarios.FindAsync(3);
            Assert.IsNull(usuarioEnBaseDeDatos);
        }
    }
}
