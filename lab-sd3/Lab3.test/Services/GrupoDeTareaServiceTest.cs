﻿using Lab3.Context;
using Lab3.Models;
using Lab3.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.test.Services
{
    [TestFixture]
    public class GrupoDeTareaServiceTest
    {
        private AppDbContext _context;
        private GrupoDeTareaService _grupoDeTareaService;
        [SetUp]
        public void Setup()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: "TestDatabase")
                .Options;

            _context = new AppDbContext(options);
            _grupoDeTareaService = new GrupoDeTareaService(_context);
        }
        [Test]
        public async Task ObtenerTodosLosGruposDeTareas()
        {
            _context.GrupoDeTareas.RemoveRange(_context.GrupoDeTareas);
            await _context.SaveChangesAsync();
            // Act
            var resultado = await _grupoDeTareaService.ObtenerTodosLosGruposDeTareas();

            // Assert
            Assert.IsNotNull(resultado);
            Assert.IsEmpty(resultado);
        }

        [Test]
        public async Task CrearGrupoDeTareas()
        {
            // Arrange
            var nuevoGrupo = new GrupoDeTareas
            {
                Id = 1,
                Nombre = "Grupo de Tareas 1",
                UsuarioId= 1,
            };

            // Act
            var resultado = await _grupoDeTareaService.CrearGrupoDeTareas(nuevoGrupo);

            // Assert
            Assert.IsNotNull(resultado);
            Assert.AreEqual(nuevoGrupo.Nombre, resultado.Nombre);
            Assert.AreEqual(nuevoGrupo.UsuarioId, resultado.UsuarioId);
        }

        [Test]
        public async Task EliminarGrupoDeTareas()
        {
            // Arrange
            var nuevoGrupo = new GrupoDeTareas
            {
                Id = 2,
                Nombre = "Grupo de Tareas 1",
                UsuarioId = 1,
            };
            await _context.GrupoDeTareas.AddAsync(nuevoGrupo);
            await _context.SaveChangesAsync();

            // Act
            await _grupoDeTareaService.EliminarGrupoDeTareas(2);

            // Assert
            var grupoEliminado = await _context.GrupoDeTareas.FindAsync(2);
            Assert.IsNull(grupoEliminado);
        }
    }
}
